## Soil Moisture Monitoring 

This project was with a partner. We used a raspberry pi to monitor the moisture in soil that can be used in a garden.
The code for detecting moisture is written in python. It would read back to you that moisture was detected or wasn't detected. 
The code for recording the data and keeping a record of the date and time is written in java.
We also had an LED light that would light up when moisture was detected.