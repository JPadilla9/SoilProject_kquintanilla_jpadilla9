import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.text.*;
import java.io.*;
import java.util.Date;
import java.util.Calendar;
import java.text.*;
import java.lang.*;
import java.util.regex.*;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

class smoisture implements ActionListener {
	JFrame moistureLevel = new JFrame("Moisture Level Database");
	JPanel moistureBoard = new JPanel(new GridLayout(0,3));
	//JPanel two = new JPanel();
	JButton exit = new JButton("exit");
	JButton update = new JButton("Update");
	JTextArea newlevel = new JTextArea(3,3);
	JTextArea newdate = new JTextArea(3,3);
	JTextArea newtime = new JTextArea(3,3);
	JLabel level = new JLabel("Soil Level");
	JLabel date = new JLabel("Date");
	JLabel time = new JLabel("Time");
	//JScrollPane scroll = new JScrollPane(newtime);

public smoisture() {
		moistureLevel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		moistureLevel.setSize(700,1000);
		//moistureBoard.setBounds(60,60,300,300);
		//two.setBound20,20,150,150);
		level.setPreferredSize(new Dimension(7,3));
		level.setOpaque(true);
		level.setBackground(new Color(255, 255, 0));
		level.setHorizontalAlignment(JLabel.CENTER);
		date.setPreferredSize(new Dimension(7,3));		
		date.setOpaque(true);
		date.setBackground(new Color(0,255, 0));
		date.setHorizontalAlignment(JLabel.CENTER);
		time.setPreferredSize(new Dimension(7,3));
		time.setOpaque(true);
		time.setBackground(new Color(255,0,255));
		time.setHorizontalAlignment(JLabel.CENTER);
		
		newlevel.setLineWrap(true);
		newlevel.setEditable(false);
		newdate.setLineWrap(true);
		newdate.setEditable(false);
		newtime.setLineWrap(true);
		newtime.setEditable(false);

		exit.addActionListener(this);
		update.addActionListener(this);
		
		//scroll.setRowHeaderView(newtime);
		moistureBoard.add(level);
		moistureBoard.add(date);
		moistureBoard.add(time);
		moistureBoard.add(newlevel);
		moistureBoard.add(newdate);
		moistureBoard.add(newtime);
		//moistureBoard.add(scroll);
		//moistureBoard.add(newtime);
		moistureBoard.add(update);
		moistureBoard.add(exit);
		// scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
          	//scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		moistureLevel.setContentPane(moistureBoard);
		moistureLevel.setVisible(true);
		moistureLevel.pack();
		newlevel.setVisible(false);
		newdate.setVisible(false);
		newtime.setVisible(false);
	}

public void actionPerformed(ActionEvent e) {
	if (e.getSource() == exit) {
		moistureLevel.dispose();
	} else if (e.getSource() == update) {

			try {
			String file_path = "/home/pi/project/newdata.txt";
			FileReader read = new FileReader(file_path);
			BufferedReader in = new BufferedReader(read);
			String line;
			List<String> allMatches = new ArrayList<String>();
			List<String> timeMatch = new ArrayList<String>();
			List<String> detection = new ArrayList<String>();
			
			while ((line = in.readLine()) != null) {
						Pattern p = Pattern.compile("(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\\s+\\d{1,2}");
						Matcher m = p.matcher(line);
						Pattern time = Pattern.compile("([01]\\d|2[0-3]):?([0-5]\\d)");
						Matcher ti = time.matcher(line);
						Pattern water = Pattern.compile("(\\bWater\\b)(.*?)(\\bDetected\\b)");
						Matcher wa = water.matcher(line);
				
						if (m.find()) {
							allMatches.add(m.group() + "\n");
							String formattedString = allMatches.toString()
								.replace(",","")
								.replace("[", "")
								.replace("]", "")
								.trim();
							
							newdate.setText(formattedString);
							newdate.setVisible(true);
						}
						if (ti.find()) {
							timeMatch.add(ti.group() + "\n");
							String formattedString2 = timeMatch.toString()
								.replace(",", "")
								.replace("[","")
								.replace("]","")
								.trim();
								
							newtime.setText(formattedString2);
							newtime.setVisible(true);
						}
						if (wa.find()) {
							detection.add(wa.group() + "\n");
							String formattedString3 = detection.toString()
								.replace(",", "")
								.replace("[", "")
								.replace("]", "")
								.trim();
								
							newlevel.setText(formattedString3);
							newlevel.setVisible(true);
						}
							
			}
			}
							
		 catch (IOException ex) {
			System.out.println("cannot load file");
		}
		//catch (ParseException exx) {
			//System.out.println("cannot parse");
}}

public static void main (String[] args) {
	new smoisture();
}}
