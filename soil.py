#!/usr/bin/python



# This file will grab the levels from the hygrometer
# and then write out the levels to a text file




# This is currently purely from reference (need to find the link)
# BUT to do: 

# Change moisture detection levels - should detect levels rather than just the existence of moisture
# Write w/ expressions that utlize quantitative monitoring but tranlates to like LOW, doin ok!!!! , HIGH
# u get the picture... 

# i don't want a simple if else statement, it needs to be more specific than that is all 

# looks like we need something for ADC if we wanted numeric levels and not just a Y/N
# also, links for python file i/o: 
	# https://www.tutorialspoint.com/python/python_files_io.htm
	# https://www.javatpoint.com/python-files-io
	# fileObject = open(file_name [, access_mode] [, buffering])



import RPi.GPIO as GPIO
import time
from time import sleep
import os
from gpiozero import LED
from gpiozero import PWMLED
from signal import pause

#GPIO SETUP
channel = 17 
GPIO.setmode(GPIO.BCM)
GPIO.setup(channel, GPIO.IN)
led = PWMLED(27)

# Output will include date and time of most recent update file 
# set variables for date & time to include in the output? 


# fileObject = open("soilmoisture.txt", "w+") using a for append also 
# add a newline character at the end???? depends
# write moisture detection to the file with .write 
# fileObject.write("the print methods from below")


localtime = time.asctime( time.localtime(time.time()) )
print GPIO.input(channel)

def callback(channel):
    	fileObject = open("newdata.txt", "a+")
	if GPIO.input(channel):
		print "Water Not Detected \n"
        	fileObject.write("Water Not Detected " + localtime + "\n")
		led.off()

	else:
		print "Water Detected \n"
                fileObject.write("Water Detected " + localtime + "\n")
		led.on()


GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=200)  # let us know when the pin goes HIGH or LOW
GPIO.add_event_callback(channel, callback)  # assign function to GPIO PIN, Run function on change

# infinite loop
while True:
	time.sleep(15);

fileObject.close();
